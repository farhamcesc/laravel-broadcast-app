<?php

use App\Events\MessageCreated;
use App\Events\MessageSent;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TelegramBotController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    //MessageCreated::dispatch('Here is where you can register web routes From Route');

    return view('welcome');
});

Route::get('/message/created', function () {

    MessageSent::dispatch('Here is where you can register web routes From Route');
    broadcast(new MessageSent('Here is where you can register web routes From Route'));

   // return view('welcome');
});

Route::get('/message/chat', function () {

    // event(new MessageSent('Hello, world!'));
    MessageSent::dispatch('Halo, Word');

});


Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login_store', [LoginController::class, 'store'])->name('login.store');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');
Route::get('/chat/{email?}', [ChatController::class, 'index'])->name('chat')->middleware('auth');
Route::post('/chat/ajax_push_chat', [ChatController::class, 'ajax_push_chat'])->name('chat.push-chat')->middleware('auth');
Route::post('/chat/ajax_get_chat', [ChatController::class, 'ajax_get_chat'])->name('chat.get-chat')->middleware('auth');


Route::post('/telegram/webhook', [TelegramBotController::class, 'webhook'])->name('webhook');
Route::get('/telegram', [TelegramBotController::class, 'index'])->name('index')->name('telegram.index')->middleware('auth');
Route::post('/telegram/send_message', [TelegramBotController::class, 'sendMessage'])->name('telegram.send_message');

Route::get('/journal', [JournalController::class, 'index'])->name('journal');
