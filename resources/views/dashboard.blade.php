@extends('template')

@section('content')
<h1>Welcome, {{ $authUser->name }}</h1>
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Server Status</h6>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    @if ($dataStatus->getData())
                    <li class="list-group-item">Subscription : {{ $dataStatus->getData()->subscription_count }}</li>
                    <li class="list-group-item">Uptime : {{ $dataStatus->getData()->uptime }}</li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Channel</h6>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    @if ($dataChannel->getData())
                    @foreach ($dataChannel->getData()->channels as $key => $item)
                    <li class="list-group-item"><b>{{ $key }}</b> <br> Subscription : {{ $item->subscription_count }} </li>
                    @endforeach

                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
