@extends('template')

@section('content')
<h1>Chat Page</h1>
<div class="row">

    <div class="col-lg-4">



        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List User</h6>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    @foreach ($dataUser as $item)
                    <li class="list-group-item"><a href="{{ route('chat',  $item->email )}}">{{ $item->name}}</a></li>
                    @endforeach
                </ul>
                @if ($getDataUserChat != null)
                <br>
                <a href="#" class="btn btn-primary btn-icon-split btn-block" data-toggle="modal" data-target="#exampleModal">
                    <span class="text">Send Message</span>
                </a>
                @endif
            </div>
        </div>

    </div>
    <div class="col-lg-8">

        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Chat With : {{ ($getDataUserChat != null ) ? $getDataUserChat->name : "-" }}</h6>
            </div>
            <input type="hidden" id="chat_key" value="{{ $chatKey }}" />
            <div class="card-body" data-spy="scroll" data-offset="0" id='card-body-chat'>
                {{-- <div class="card mb-2 border-left-primary">
                    <div class="card-body">
                        <b>Nama : </b>
                        <br>
                        Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling.
                         Laravel takes the pain out of development by easing common tasks used in many web projects, such as:
                    </div>
                </div>
                <div class="card mb-2 border-left-danger">
                    <div class="card-body">
                        <b>Nama : </b>
                        <br>
                        Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling.
                         Laravel takes the pain out of development by easing common tasks used in many web projects, such as:
                    </div>
                </div> --}}

            </div>
        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Send Message</h5>
            <form id="ajaxPushChat">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group">

                    <input type="hidden" id="email_from" value="{{ $authUser->email }}" />
                    <input type="hidden" id="email_to" value="{{ ($getDataUserChat != null ) ? $getDataUserChat->email : null }}" />
                    <textarea class="form-control" id="message" rows="5"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
            </form>
        </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script src="/js/app.js">

    </script>
    <script>

        function getDataChat(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#card-body-chat').html('');

            var formData = {
                email_from: $('#email_from').val(),
                email_to: $('#email_to').val()
            };

            // Send AJAX POST request
            $.ajax({
                type: 'POST',
                url: '{{ route('chat.get-chat') }}', // Replace with your route
                data: formData,
                dataType: 'json',
                success: function(response) {
                    // Handle successful response
                    console.log(response.message);
                    $('#card-body-chat').html(response.data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // Handle errors here
                    console.log(textStatus);
                }
            });
        }

        // Kalo ada Pesan, Senggol GetDataChat

        $(document).ready(function() {
            //console.log(window.location.hostname)
            let chat_key = $('#chat_key').val();
            let listen  = 'event_broadcast_'+chat_key;
            console.log(listen);
            window.Echo.channel("farham-channel").listen(
                "."+listen,
                (response) => {
                    console.log(response);
                    let email_from = $('#email_from').val();
                    let message = '';
                    if(email_from == response.from){
                        message = response.message_from
                    }else{
                        message = response.message_to
                    }
                    $('#card-body-chat').append(message);
                }
            );
            getDataChat()
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#ajaxPushChat').submit(function(event) {
                event.preventDefault(); // Prevent the form from submitting normally

                // Get form data
                var formData = {
                    email_from: $('#email_from').val(),
                    email_to: $('#email_to').val(),
                    message: $('#message').val()
                };

                // Send AJAX POST request
                $.ajax({
                    type: 'POST',
                    url: '{{ route('chat.push-chat') }}', // Replace with your route
                    data: formData,
                    dataType: 'json',
                    success: function(response) {
                        // Handle successful response
                        console.log(response.message);
                        $('#message').val('');
                        $('#exampleModal').modal('hide');

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Handle errors here
                        console.log(textStatus);
                    }
                });
            });
        });
    </script>
@endpush
