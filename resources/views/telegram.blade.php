@extends('template')

@section('content')
<h1>Chat Page</h1>
<div class="row">

    <div class="col-lg-4">

        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List User</h6>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('telegram.send_message')}}">
                    @csrf
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea class="form-control" name="pesan" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                    </form>
            </div>
        </div>

    </div>
</row>
@endsection
