<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Sushi\Sushi;

class Journals extends Model
{
    use HasFactory, Sushi;

    public function getRows()
    {
        $url = 'https://script.googleusercontent.com/macros/echo?user_content_key=p5BZx9LEeENbHr17mX29V68f_Kjr330-t-yGbyqKsbiZW9cYWurYvogQ0aeLu1bDPDSDNP9xXr5kZdU5tzzNHI4T8t5lNVyBm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnNTOqkQmdudGdg1xpk_2tgBZ4CEvHe0ay7t5t6uMjj9fQn2CZw-6EN9D9nyLJQqHlCgzd3jUu8oTcLKrYevE-inGP6ZqNBeINNz9Jw9Md8uu&lib=M4FgxqNKmfb7NrYJAQbn5TorfjWmsbXYa';
        $journal = Http::get($url)->json();

        $data = Arr::map($journal['data'], function($item) {
            return Arr::only($item,
            [
                'tanggal',
                'keterangan',
                'kredit'
            ]);
        });

        return $data;
    }

    protected function sushiShouldCache()
    {
        return true;
    }
}
