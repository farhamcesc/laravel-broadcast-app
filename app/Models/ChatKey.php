<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatKey extends Model
{
    use HasFactory;

    protected $table = 'chatkeys';
    protected $fillable = ['from', 'to', 'chat_key', 'created_at', 'updated_at'];
}
