<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $table = 'chats';
    protected $fillable = ['from', 'name_from', 'to', 'name_to', 'message', 'chat_key', 'created_at', 'updated_at'];
}
