<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class SetWebhook extends Command
{
    protected $signature = 'telegram:set-webhook';
    protected $description = 'Set the Telegram bot webhook';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //$url = route('webhook');
        $url = "http://localhost:8000/telegram/webhook";
        $response = Telegram::setWebhook(['url' => $url]);

        if ($response['ok']) {
            $this->info('Webhook set successfully');
        } else {
            $this->error('Failed to set webhook');
        }
    }
}
