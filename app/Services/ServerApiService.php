<?php
namespace App\Services;

use GuzzleHttp\Client;

class ServerApiService{

    private $host;
    private $id;
    private $key;
    private $event;
    private $channel;

    public function __construct()
    {
        $this->host = env('PUSHER_HOST');
        $this->id = env('PUSHER_APP_ID');
        $this->key = env('PUSHER_APP_KEY');
    }

    public function informationStatus(){
        $client = new Client();

        $response = $client->get($this->host.':6001/apps/'.$this->id.'/status?auth_key='.$this->key);

        $data = json_decode($response->getBody()->getContents(), true);

        return response()->json($data);
    }

    public function informationChannel(){
        $client = new Client();

        $response = $client->get($this->host.':6001/apps/'.$this->id.'/channels?auth_key='.$this->key);

        $data = json_decode($response->getBody()->getContents(), true);

        return response()->json($data);
    }

    public function triggerEvent()
    {
        $client = new Client();
        $response = $client->post( $this->host.':6001/apps/'.$this->id.'/events', [
            'json' => [
                'channels' => ['your-channel-name'],
                'name' => 'your-event-name',
                'data' => ['message' => 'hello world'],
            ],
            'query' => [
                'auth_key' =>  $this->key,
            ],
        ]);

        return response()->json(json_decode($response->getBody()->getContents()));
    }
}
