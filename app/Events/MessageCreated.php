<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chat_key;
    public $from;
    public $to;
    public $message_from;
    public $message_to;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($chat_key, $from, $to, $message_from, $message_to)
    {
        //
        $this->chat_key = $chat_key;
        $this->from = $from;
        $this->to = $to;
        $this->message_from = $message_from;
        $this->message_to = $message_to;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('messages');

        $channel = 'message_'.$this->chat_key;
        return new Channel('farham-channel');
    }

    public function broadcastAs()
    {
        $boardcastEvent = 'event_broadcast_'.$this->chat_key;
        return $boardcastEvent;
    }

    public function broadcastWith()
    {
        return [
            'chat_key' => $this->chat_key,
            'from' => $this->from,
            'to' => $this->to,
            'message_from' => $this->message_from,
            'message_to' => $this->message_to
        ];
    }

}
