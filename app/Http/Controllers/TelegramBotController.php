<?php

namespace App\Http\Controllers;

use App\Services\TelegramService;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramBotController extends Controller
{
    public function index(){
        return view('telegram');
    }
    public function webhook(Request $request)
    {
        $update = Telegram::commandsHandler(true);

        // Add your bot logic here
        $chatId = $update->getMessage()->getChat()->getId();
        $text = $update->getMessage()->getText();

        if ($text === '/start') {
            $responseText = "Hello! Welcome to our bot.";
        } else {
            $responseText = "You said: " . $text;
        }

        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $responseText
        ]);

        return response('OK', 200);
    }

    public function sendMessage(Request $request)
    {
        $telegram = new TelegramService();
        $chatId = env('TELEGRAM_BOT_CHAT_ID');
        $message = $request->input('pesan');

        //dd($request->all());

        $reponse = $telegram->sendMessage($chatId, $message);
        dd($reponse);

        return redirect()->back();
    }


}
