<?php

namespace App\Http\Controllers;

use App\Events\MessageCreated;
use App\Models\Chat;
use App\Models\ChatKey;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ChatController extends Controller
{
    //

    public function index($email = null)
    {
        //
        $authUser = Auth::user();
        $dataUser = User::where('email', '!=', $authUser->email)->get();
        $chatKey = '';
        if($email != null){
            $getDataUserChat = User::where('email', $email)->first();

            // Insert Chat Keys
            $getChatKeyFromTo =  ChatKey::where('from', $authUser->email)->where('to', $email)->first();
            $getChatKeyToFrom =  ChatKey::where('from', $email)->where('to', $authUser->email)->first();

           // dd($getChatKeyFromTo);

            if($getChatKeyFromTo == null && $getChatKeyToFrom == null){
                // Create UUID Baru
                $chat_key_uuid = Str::uuid();
                $firstOrCreate = ChatKey::firstOrCreate(
                    [
                        'from' => $authUser->email,
                        'to' => $email
                    ],
                    [
                        'from' => $authUser->email,
                        'to' => $email,
                        'chat_key' => $chat_key_uuid,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                    );
                $chatKey = $chat_key_uuid;
            }elseif($getChatKeyFromTo == null && $getChatKeyToFrom != null){
                // Get UUD from getChatKeyToFrom
                $firstOrCreate = ChatKey::firstOrCreate(
                    [
                        'from' => $authUser->email,
                        'to' => $email
                    ],
                    [
                        'from' => $authUser->email,
                        'to' => $email,
                        'chat_key' => $getChatKeyToFrom->chat_key,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                    );
                $chatKey = $getChatKeyToFrom->chat_key;
            }elseif($getChatKeyFromTo != null && $getChatKeyToFrom == null){
                // Get UUD from getChatKeyFromTo
                $firstOrCreate = ChatKey::firstOrCreate(
                    [
                        'from' => $email,
                        'to' => $authUser->email
                    ],
                    [
                        'from' => $email,
                        'to' => $authUser->email,
                        'chat_key' => $getChatKeyFromTo->chat_key,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                    );
                $chatKey = $getChatKeyFromTo->chat_key;
            }else{
                $chatKey = $getChatKeyFromTo->chat_key;
            }

        }else{
            $getDataUserChat = null;
        }

        return view('chat', compact('dataUser', 'getDataUserChat', 'authUser', 'chatKey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function ajax_push_chat(Request $request){
        $request->validate([
            'email_from' => 'required|string|max:255',
            'email_to' => 'required|string|max:255',
            'message' => 'required|string',
        ]);

        $getUserFrom = User::where('email', $request->email_from)->first();
        $getUserTo = User::where('email', $request->email_to)->first();
        $getChatKey = ChatKey::where('from',  $request->email_from)->where('to', $request->email_to)->first();

        $insertChat = Chat::create([
            'from' => $request->email_from,
            'name_from' => $getUserFrom->name,
            'to' => $request->email_to,
            'name_to' => $getUserTo->name,
            'message' => $request->message,
            'chat_key' => $getChatKey->chat_key,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $chat_from = '<div class="card mb-2 border-left-primary">
                    <div class="card-body text-right">
                        <b>'.$insertChat->name_from.'</b>
                        <br>
                    '.$insertChat->message.'
                    </div>
                </div>';
        $chat_to = '<div class="card mb-2 border-left-danger">
                    <div class="card-body">
                        <b>'.$insertChat->name_from.'</b>
                        <br>
                    '.$insertChat->message.'
                    </div>
                </div>';
        $data = $chat_from;
        MessageCreated::dispatch($getChatKey->chat_key, $request->email_from, $request->email_to, $chat_from, $chat_to);

        return response()->json(['data' => $data, 'message' => 'Berhasil Chat ']);
    }

    public function ajax_get_chat(Request $request){
        $request->validate([
            'email_from' => 'required|string|max:255',
            'email_to' => 'required|string|max:255',
        ]);
        $data = null;
        $dataUser = Auth::user();

        $getChatKey = ChatKey::where('from', $request->email_from)->where('to', $request->email_to)->first();
        $getDataChat = Chat::where('chat_key', $getChatKey->chat_key)->get();

        foreach ($getDataChat as $key => $value) {
            # code...
            if($value->from == $dataUser->email){
                $boderType = 'border-left-primary';
                $name = $value->name_from;
                $text_align = 'text-right';
            }else{
                $boderType = 'border-left-danger';
                $name = $value->name_from;
                $text_align = '';
            }
            $data .= '<div class="card mb-2 '.$boderType.'">
                        <div class="card-body '.$text_align.'">
                            <b>'.$name.'</b>
                            <br>
                           '.$value->message.'
                        </div>
                    </div>';
        }

        return response()->json([
            'data' => $data,
            'message' => 'Form data received successfully!']);
    }
}
